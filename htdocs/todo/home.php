<?php

session_start();

if(!isset($_SESSION["todoCollection"]))
$_SESSION["todoCollection"] = [];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="todo.css">
	<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
	<form method = "POST" action="/submit.php">
	<div class="mb-3 todo">
  <label for="formGroupExampleInput" class="form-label">Enter Todo</label>
  <input type="text" name="item" class="form-control" id="formGroupExampleInput" placeholder="Enter the todo">
</div>
		
		
		<button style="display:none" type="submit">Submit</button>
	</form>
	<div>
	<ul>
   <?php for($i = 0; $i < sizeof($_SESSION["todoCollection"]); $i++) { ?>
        <li><?php echo $_SESSION["todoCollection"][$i]["caption"]; ?>
		<input class="btn btn-danger"type="button" value="remove" 
        onclick=" location.href = '/remove.php?index=<?php echo $i; ?>';">
    <?php } ?></li>
		
</ul>

	</div>
</body>
</html>